---- Minecraft Crash Report ----

WARNING: coremods are present:
  DLFMLCorePlugin (DynamicLights_1.12.2.jar)
  IELoadingPlugin (ImmersiveEngineering-core-0.12-98.jar)
  Inventory Tweaks Coremod (InventoryTweaks_1.12.2_1.64dev.151.jar)
  XaeroMinimapPlugin (Xaeros_Minimap_24.1.0_Forge_1.12.jar)
  EnderCorePlugin (EnderCore-1.12.2-0.5.78-core.jar)
Contact their authors BEFORE contacting forge

// Don't be sad, have a hug! <3

Time: 4/20/24 10:33 PM
Description: Exception in server tick loop

java.lang.VerifyError: Inconsistent stackmap frames at branch target 47
Exception Details:
  Location:
    net/minecraft/world/World.func_175638_a(Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/world/EnumSkyBlock;)I @32: goto
  Reason:
    Current frame's stack size doesn't match stackmap.
  Current Frame:
    bci: @32
    flags: { }
    locals: { 'net/minecraft/world/World', 'net/minecraft/util/math/BlockPos', 'net/minecraft/world/EnumSkyBlock', 'net/minecraft/block/state/IBlockState' }
    stack: { integer }
  Stackmap Frame:
    bci: @47
    flags: { }
    locals: { 'net/minecraft/world/World', 'net/minecraft/util/math/BlockPos', 'net/minecraft/world/EnumSkyBlock', 'net/minecraft/block/state/IBlockState' }
    stack: { top, integer }
  Bytecode:
    0x0000000: 2cb2 0281 a600 0e2a 2bb6 02fb 9900 0610
    0x0000010: 0fac 2a2b b601 824e 2cb2 0281 a600 0703
    0x0000020: a700 0f2d b901 9201 002d 2a2b b808 5b36
    0x0000030: 042d b901 9201 002d 2a2b b602 fe36 0515
    0x0000040: 0504 a200 0604 3605 1505 100f a100 0615
    0x0000050: 04ac 1504 100e a100 0615 04ac b804 b13a
    0x0000060: 06b8 085f 3a07 1907 be36 0803 3609 1509
    0x0000070: 1508 a200 4a19 0715 0932 3a0a 1906 2bb6
    0x0000080: 0865 190a b608 6957 2a2c 1906 b603 3415
    0x0000090: 0564 360b 150b 1504 a400 0715 0b36 0415
    0x00000a0: 0410 0ea1 0013 1504 360c 150c 360d 1906
    0x00000b0: b604 bc15 0dac 8409 01a7 ffb5 1504 3607
    0x00000c0: 1906 b604 bc15 07ac 3a0e 1906 b604 bc19
    0x00000d0: 0ebf                                   
  Exception Handler Table:
    bci [97, 174] => handler: 200
    bci [182, 192] => handler: 200
    bci [200, 202] => handler: 200
  Stackmap Table:
    same_frame(@18)
    append_frame(@35,Object[#398])
    full_frame(@47,{Object[#2],Object[#23],Object[#637],Object[#398]},{Top,Integer})
    full_frame(@72,{Object[#2],Object[#23],Object[#637],Object[#398],Integer,Integer},{Top})
    same_locals_1_stack_item_frame(@82,Top)
    same_locals_1_stack_item_frame(@92,Top)
    full_frame(@110,{Object[#2],Object[#23],Object[#637],Object[#398],Integer,Integer,Object[#21],Object[#2145],Integer,Integer},{Top})
    full_frame(@159,{Object[#2],Object[#23],Object[#637],Object[#398],Integer,Integer,Object[#21],Object[#2145],Integer,Integer,Object[#683],Integer},{Top})
    same_locals_1_stack_item_frame(@182,Top)
    full_frame(@188,{Object[#2],Object[#23],Object[#637],Object[#398],Integer,Integer,Object[#21],Object[#2145],Integer,Integer},{Top})
    full_frame(@200,{Object[#2],Object[#23],Object[#637],Object[#398],Integer,Integer,Object[#21]},{Object[#271]})

	at net.minecraft.server.integrated.IntegratedServer.func_71247_a(IntegratedServer.java:132)
	at net.minecraft.server.integrated.IntegratedServer.func_71197_b(IntegratedServer.java:233)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:486)
	at java.lang.Thread.run(Unknown Source)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 11 (amd64) version 10.0
	Java Version: 1.8.0_411, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 4660287944 bytes (4444 MB) / 7827095552 bytes (7464 MB) up to 7827095552 bytes (7464 MB)
	JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms8096m -Xmx8096m
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2859 Optifine OptiFine_1.12.2_HD_U_G5 73 mods loaded, 73 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State  | ID                                | Version              | Source                                             | Signature                                |
	|:------ |:--------------------------------- |:-------------------- |:-------------------------------------------------- |:---------------------------------------- |
	| LCHIJA | minecraft                         | 1.12.2               | minecraft.jar                                      | None                                     |
	| LCHIJA | mcp                               | 9.42                 | minecraft.jar                                      | None                                     |
	| LCHIJA | FML                               | 8.0.99.99            | forge-1.12.2-14.23.5.2859-universal.jar            | e3c3d50c7c986df74c645c0ac54639741c90a557 |
	| LCHIJA | forge                             | 14.23.5.2859         | forge-1.12.2-14.23.5.2859-universal.jar            | e3c3d50c7c986df74c645c0ac54639741c90a557 |
	| LCHIJA | xaerominimap_core                 | 1.12.2-1.0           | minecraft.jar                                      | None                                     |
	| LCHIJA | appliedenergistics2               | rv6-stable-7         | appliedenergistics2_rv6_stable_7.jar               | None                                     |
	| LCHIJA | baubles                           | 1.5.2                | Baubles_1.12_1.5.2.jar                             | None                                     |
	| LCHIJA | betterbuilderswands               | 0.13.1               | BetterBuildersWands_1.12.2_0.13.1.26913450ff.jar   | None                                     |
	| LCHIJA | bettercaves                       | 1.12.2               | bettercaves_1.12.2_2.0.4.jar                       | None                                     |
	| LCHIJA | jei                               | 4.16.1.1012          | jei_1.12.2-4.16.1.1012.jar                         | None                                     |
	| LCHIJA | botania                           | r1.10-364            | Botania-1.12.2-r1.10-364.4.jar                     | None                                     |
	| LCHIJA | codechickenlib                    | 3.2.3.358            | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | f1850c39b2516232a2108a7bd84d1cb5df93b261 |
	| LCHIJA | redstoneflux                      | 2.1.1                | RedstoneFlux-1.12-2.1.1.1-universal.jar            | None                                     |
	| LCHIJA | brandonscore                      | 2.4.19               | BrandonsCore-1.12.2-2.4.19.214-universal.jar       | None                                     |
	| LCHIJA | buildcraftlib                     | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcraftcore                    | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcraftbuilders                | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcrafttransport               | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcraftsilicon                 | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcraftenergy                  | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | ic2                               | 2.8.222-ex112        | industrialcraft-2-2.8.222-ex112.jar                | de041f9f6187debbc77034a344134053277aa3b0 |
	| LCHIJA | forestry                          | 5.8.2.414            | forestry_1.12.2_5.8.2.414.jar                      | None                                     |
	| LCHIJA | buildcraftcompat                  | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcraftfactory                 | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | buildcraftrobotics                | 7.99.24.8            | buildcraft_all_7.99.24.8_1.12.2.jar                | None                                     |
	| LCHIJA | chiselsandbits                    | 14.32                | chiselsandbits_1.12.2_14.32.jar                    | None                                     |
	| LCHIJA | cofhcore                          | 4.6.6                | CoFHCore-1.12.2-4.6.6.1-universal.jar              | None                                     |
	| LCHIJA | cofhworld                         | 1.4.0                | CoFHWorld-1.12.2-1.4.0.1-universal.jar             | None                                     |
	| LCHIJA | cucumber                          | 1.1.3                | Cucumber-1.12.2-1.1.3.jar                          | None                                     |
	| LCHIJA | customnpcs                        | 1.12                 | CustomNPCs_1.12.2_05Jul20.jar                      | None                                     |
	| LCHIJA | cyclicmagic                       | 1.20.12              | Cyclic-1.12.2-1.20.14.jar                          | 0e5cb559be7d03f3fc18b8cba547d663e25f28af |
	| LCHIJA | divinerpg                         | 1.7.1                | DivineRPG_1.12.2_1.7.1.jar                         | None                                     |
	| LCHIJA | thermalfoundation                 | 2.6.7                | ThermalFoundation-1.12.2-2.6.7.1-universal.jar     | None                                     |
	| LCHIJA | draconicevolution                 | 2.3.27               | Draconic-Evolution-1.12.2-2.3.27.353-universal.jar | None                                     |
	| LCHIJA | dynamiclights                     | 1.4.8                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_onfire              | 1.0.7                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_creepers            | 1.0.6                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_dropitems           | 1.1.0                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_entityclasses       | 1.0.1                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_mobequipment        | 1.1.0                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_flamearrows         | 1.0.1                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_floodlights         | 1.0.3                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_otherplayers        | 1.0.9                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamiclights_theplayer           | 1.1.3                | DynamicLights_1.12.2.jar                           | None                                     |
	| LCHIJA | dynamictrees                      | 1.12.2-0.9.29        | DynamicTrees-1.12.2-0.9.29.jar                     | None                                     |
	| LCHIJA | endercore                         | 1.12.2-0.5.78        | EnderCore-1.12.2-0.5.78.jar                        | None                                     |
	| LCHIJA | thermalexpansion                  | 5.5.7                | ThermalExpansion-1.12.2-5.5.7.1-universal.jar      | None                                     |
	| LCHIJA | enderio                           | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderiointegrationtic             | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderiobase                       | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderioconduits                   | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderioconduitsappliedenergistics | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderioconduitsopencomputers      | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderioconduitsrefinedstorage     | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderiointegrationforestry        | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderiointegrationticlate         | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderioinvpanel                   | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderiomachines                   | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | enderiopowertools                 | 5.3.72               | EnderIO-1.12.2-5.3.72.jar                          | None                                     |
	| LCHIJA | cfm                               | 6.3.0                | Furniture-Mod-6.3.2-1.12.2.jar                     | None                                     |
	| LCHIJA | gravestone                        | 1.10.0               | gravestone_mc1.12.2_1.10.0.jar                     | None                                     |
	| LCHIJA | waila                             | 1.8.26               | Hwyla_1.8.26_B41_1.12.2.jar                        | None                                     |
	| LCHIJA | inventorytweaks                   | 1.64+dev.151.822d839 | InventoryTweaks_1.12.2_1.64dev.151.jar             | 55d2cd4f5f0961410bf7b91ef6c6bf00a766dcbe |
	| LCHIJA | ironchest                         | 1.12.2-7.0.67.844    | ironchest_1.12.2_7.0.72.847.jar                    | None                                     |
	| LCHIJA | mca                               | 6.1.0                | MCA_1.12.2_6.1.0_universal.jar                     | None                                     |
	| LCHIJA | mysticalagriculture               | 1.7.5                | MysticalAgriculture-1.12.2-1.7.5.jar               | None                                     |
	| LCHIJA | prefab                            | 1.3.1.7              | prefab-1.12.2-1.3.1.7.jar                          | None                                     |
	| LCHIJA | srparasites                       | 1.9.18               | SRParasites-1.12.2v1.9.18.jar                      | None                                     |
	| LCHIJA | twilightforest                    | 3.11.1021            | twilightforest_1.12.2_3.11.1021_universal.jar      | None                                     |
	| LCHIJA | uteamcore                         | 2.2.5.147            | u_team_core-1.12.2-2.2.5.147.jar                   | None                                     |
	| LCHIJA | usefulbackpacks                   | 1.5.4.85             | useful_backpacks-1.12.2-1.5.4.85.jar               | None                                     |
	| LCHIJA | xaerominimap                      | 24.1.0               | Xaeros_Minimap_24.1.0_Forge_1.12.jar               | None                                     |
	| LCHIJA | immersiveengineering              | 0.12-98              | ImmersiveEngineering_0.12_98.jar                   | None                                     |

	Loaded coremods (and transformers): 
DLFMLCorePlugin (DynamicLights_1.12.2.jar)
  atomicstryker.dynamiclights.common.DLTransformer
IELoadingPlugin (ImmersiveEngineering-core-0.12-98.jar)
  blusunrize.immersiveengineering.common.asm.IEClassTransformer
Inventory Tweaks Coremod (InventoryTweaks_1.12.2_1.64dev.151.jar)
  invtweaks.forge.asm.ContainerTransformer
XaeroMinimapPlugin (Xaeros_Minimap_24.1.0_Forge_1.12.jar)
  xaero.common.core.transformer.ChunkTransformer
  xaero.common.core.transformer.NetHandlerPlayClientTransformer
  xaero.common.core.transformer.EntityPlayerTransformer
  xaero.common.core.transformer.AbstractClientPlayerTransformer
  xaero.common.core.transformer.WorldClientTransformer
  xaero.common.core.transformer.EntityPlayerMPTransformer
  xaero.common.core.transformer.EntityPlayerSPTransformer
  xaero.common.core.transformer.PlayerListTransformer
  xaero.common.core.transformer.SaveFormatTransformer
  xaero.common.core.transformer.GuiIngameForgeTransformer
  xaero.common.core.transformer.MinecraftServerTransformer
  xaero.common.core.transformer.GuiBossOverlayTransformer
  xaero.common.core.transformer.ModelRendererTransformer
EnderCorePlugin (EnderCore-1.12.2-0.5.78-core.jar)
  com.enderio.core.common.transform.EnderCoreTransformer
  com.enderio.core.common.transform.SimpleMixinPatcher
	GL info: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
	AE2 Version: stable rv6-stable-7 for Forge 14.23.5.2768
	Ender IO: Found the following problem(s) with your installation (That does NOT mean that Ender IO caused the crash or was involved in it in any way. We add this information to help finding common problems, not as an invitation to post any crash you encounter to Ender IO's issue tracker. Always check the stack trace above to see which mod is most likely failing.):
                  * Optifine is installed. This is NOT supported.
                 This may (look up the meaning of 'may' in the dictionary if you're not sure what it means) have caused the error. Try reproducing the crash WITHOUT this/these mod(s) before reporting it.
	Authlib is : /C:/Users/user/Desktop/MultiMC/libraries/com/mojang/authlib/1.5.25/authlib-1.5.25.jar

	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!!!You are looking at the diagnostics information, not at the crash.       !!!
	!!!Scroll up until you see the line with '---- Minecraft Crash Report ----'!!!
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	AE2 Integration: IC2:ON, RC:OFF, MFR:OFF, Waila:ON, InvTweaks:ON, JEI:ON, Mekanism:OFF, OpenComputers:OFF, THE_ONE_PROBE:OFF, TESLA:OFF, CRAFTTWEAKER:OFF
	Profiler Position: N/A (disabled)
	Player Count: 0 / 8; []
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fml,forge'
	OptiFine Version: OptiFine_1.12.2_HD_U_G5
	OptiFine Build: 20210124-142939
	Render Distance Chunks: 12
	Mipmaps: 4
	Anisotropic Filtering: 1
	Antialiasing: 0
	Multitexture: false
	Shaders: BSL_v8.2.07.1.zip
	OpenGlVersion: 4.6.0 NVIDIA 472.12
	OpenGlRenderer: NVIDIA GeForce RTX 3070 Ti/PCIe/SSE2
	OpenGlVendor: NVIDIA Corporation
	CpuCount: 12